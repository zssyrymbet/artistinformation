//
//  ArtistManager.swift
//  ArtistInformation
//
//  Created by Zarina Syrymbet on 4/26/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import Firebase
import FirebaseFirestore

class ArtistManager {
    
    private let ARTIST_INFORMATION = "https://theaudiodb.com/api/v1/json/1/search.php?"
    private let ARTIST_ALBUMS = "https://theaudiodb.com/api/v1/json/1/searchalbum.php?"
    private let ALBUM_DETAILS = "https://theaudiodb.com/api/v1/json/1/searchalbum.php?"
    private let TRACKS_OF_ALBUM = "https://theaudiodb.com/api/v1/json/1/track.php?"
    private let VIDEOS_OF_ARTIST = "https://theaudiodb.com/api/v1/json/1/mvid.php?"

    
    func loadArtistInformation(artistName: String, onSuccess: @escaping (ArtistResponse) -> Void, onFailure: @escaping (String) -> Void) {
        let params = [
            "s": artistName
        ]
        
        AF.request(ARTIST_INFORMATION, method: .get, parameters: params).responseDecodable { (response: DataResponse<ArtistResponse, AFError>) in
                switch response.result {
                case .success(let artistResponse):
                    onSuccess(artistResponse)
                case .failure(let error):
                    onFailure(error.localizedDescription)
                    print("ERROR ", error)
                }
        }
    }
    
    func loadArtistAlbum(artistName: String, onSuccess: @escaping (AlbumResponse) -> Void, onFailure: @escaping (String) -> Void) {
        let params = [
            "s": artistName
        ]
        
        AF.request(ARTIST_ALBUMS, method: .get, parameters: params).responseDecodable { (response: DataResponse<AlbumResponse, AFError>) in
                switch response.result {
                case .success(let albumResponse):
                    onSuccess(albumResponse)
                case .failure(let error):
                    onFailure(error.localizedDescription)
                }
        }
    }
    
    func loadAlbumDetails(albumName: String, onSuccess: @escaping (AlbumDetailsResponse) -> Void, onFailure: @escaping (String) -> Void) {
        let params = [
            "a": albumName
        ]
        
        AF.request(ALBUM_DETAILS, method: .get, parameters: params).responseDecodable { (response: DataResponse<AlbumDetailsResponse, AFError>) in
                switch response.result {
                case .success(let albumDetailResponse):
                    onSuccess(albumDetailResponse)
                case .failure(let error):
                    onFailure(error.localizedDescription)
                }
        }
    }
    
    func loadAlbumTracks(albumId: String, onSuccess: @escaping (TrackResponse) -> Void, onFailure: @escaping (String) -> Void) {
        let params = [
            "m": albumId
        ]
        
        AF.request(TRACKS_OF_ALBUM, method: .get, parameters: params).responseDecodable { (response: DataResponse<TrackResponse, AFError>) in
                switch response.result {
                case .success(let trackResponse):
                    onSuccess(trackResponse)
                case .failure(let error):
                    onFailure(error.localizedDescription)
                }
        }
    }
    
    func loadVideoByArtist(artistId: String, onSuccess: @escaping (VideoResponse) -> Void, onFailure: @escaping (String) -> Void) {
        let params = [
            "i": artistId
        ]
        
        AF.request(VIDEOS_OF_ARTIST, method: .get, parameters: params).responseDecodable { (response: DataResponse<VideoResponse, AFError>) in
                switch response.result {
                case .success(let videoResponse):
                    onSuccess(videoResponse)
                case .failure(let error):
                    print(error)
                    onFailure(error.localizedDescription)
                }
        }
    }
    
    func createUser(email: String, password: String, username: String,  onComplete: @escaping() -> Void) {
       Auth.auth().createUser(withEmail:email, password: password) { (result, error) in
             if let error = error {
               print(error)
                 return
             }
        
            onComplete()
        
           self.saveUserData(uid: result?.user.uid ?? "", email: email, username: username, password: password)
         }
    }
       
    func saveUserData(uid: String, email: String, username: String, password: String) {
       Firestore.firestore()
            .collection("users")
            .document(uid)
             .setData(
                   [
                       "id": uid,
                       "email": email,
                       "username": username,
                       "password": password,
                   ]
            )
    }
    
    func signIn(email: String, password: String, onComplete: @escaping() -> Void) {
           Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
               if let error = error {
                   print(error.localizedDescription)
                   return
               }
               
               guard let uid = result?.user.uid else {
                   return
               }
                onComplete()
                self.loadUserData(uid: uid)
           }
    }
    
    func loadUserData(uid: String) {
        Firestore.firestore()
            .collection("users")
            .document(uid)
            .addSnapshotListener { (snapshot, error) in
                if let error = error {
                    print(error.localizedDescription)
                    return
                }
            }
    }
    
    func saveToFavourites(userId: String, artist: [Artist], onComplete: @escaping() -> Void) {
        let artistDicts = artist.map({ $0.ArtistToDict() })

        Firestore.firestore()
            .collection("favourites")
            .document(userId)
            .setData(
                [
                    "userId": userId,
                    "artist": artistDicts
                ]
        ){ error in
            if error == nil {
               onComplete()
            }
        }
    }
    
    func removeFromFavourites(userId: String, artist: [Artist], onComplete: @escaping() -> Void) {
       let artistDicts = artist.map({ $0.ArtistToDict() }).first
           Firestore.firestore()
           .collection("favourites")
           .document(userId)
           .updateData(
                [
                   "artist": FieldValue.arrayRemove([artistDicts as Any])
                ]
           ){ error in
                if error != nil {
                   onComplete()
                }
            }
    }
    
     func getFavourites(userId: String, onComplete: @escaping (Favourites?) -> Void) {
           print(userId)
           Firestore.firestore()
               .collection("favourites")
               .document(userId)
               .addSnapshotListener { (snapshot, error) in
                   if let error = error {
                       print(error.localizedDescription)
                       return
                   }
                   
                   if snapshot!.data() != nil {
                       let favourite = Favourites(data: (snapshot!.data()!))
                       onComplete(favourite!)
                   } else {
                       onComplete(nil)
                   }
               }
       }


    
    func updateFavourites(userId: String, artist: [Artist], onComplete: @escaping() -> Void) {
        let artistDicts = artist.map({ $0.ArtistToDict() }).first
           Firestore.firestore()
               .collection("favourites")
               .document(userId)
               .updateData(
                    [
                        "artist": FieldValue.arrayUnion([artistDicts as Any])
                    ]
               ){ error in
                   if error == nil {
                      onComplete()
                   }
               }
       }
    
    func loadImage(url: String, onSuccess: @escaping (Data) -> Void) {
           AF.request(url).responseImage { (image) in
               if image.data != nil {
                   onSuccess(image.data!)
               }
           }
    }
    
    func loadImageLogo(url: String, onSuccess: @escaping (Data) -> Void) {
           AF.request(url).responseImage { (image) in
               if image.data != nil {
                   onSuccess(image.data!)
               }
           }
    }
    
    func signOut() -> Bool {
        do {
            try Auth.auth().signOut()
            return true
        } catch {
            return false
        }
    }    
}

