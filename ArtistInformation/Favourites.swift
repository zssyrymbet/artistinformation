//
//  Favourites.swift
//  ArtistInformation
//
//  Created by Zarina Syrymbet on 4/27/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit

struct Favourites {
    var artist: [Artist]
    var userId: String


    init?(data: [String: Any]) {
        guard let artist = data["artist"] as? [[String: Any]], let userId = data["userId"] as? String else {
                    return nil
            }
        
            self.artist = artist.map({ Artist(data: $0)! })
            self.userId = userId
    }
}

