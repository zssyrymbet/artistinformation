//
//  ViewController.swift
//  ArtistInformation
//
//  Created by Zarina Syrymbet on 4/26/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit
import Firebase


class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {

    private let manager = ArtistManager()
    var artistList = [Artist]()
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        searchBar.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        tableView.reloadData()
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        findArtist(artistName: searchBar.text!)
    }
    
    func findArtist(artistName: String) {
       manager.loadArtistInformation(artistName: artistName, onSuccess: { (artistResponse) in
           if artistResponse.artists.count != 0 {
                self.artistList = artistResponse.artists
                self.tableView.reloadData()
           }
       }) { (error) in
           print("ERROR: ", error)
       }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return artistList.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ArtistTableViewCell
                cell.configureCell(artist: artistList[indexPath.row])
                return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "ArtistViewController") {
            if let viewController = segue.destination as? ArtistInformationViewController {
                viewController.cellValue = artistList
            }
        }
    }
    
    
    @IBAction func exit(_ sender: Any) {
        if(manager.signOut()) {
            print("SIGNOUT")
            navigationController?.popViewController(animated: true)
        }
    }
    
}

