//
//  FavouritesTableViewController.swift
//  ArtistInformation
//
//  Created by Zarina Syrymbet on 5/1/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit
import Firebase

class FavouritesTableViewController: UITableViewController {

    private let manager = ArtistManager()
    var artistList: [Artist] = []


    override func viewDidLoad() {
       super.viewDidLoad()
    }
   
    override func viewDidAppear(_ animated: Bool) {
       tableView.reloadData()
    }
       
    override func viewWillAppear(_ animated: Bool) {
        artistList = []
        if Auth.auth().currentUser?.uid != nil {
            manager.getFavourites(userId: Auth.auth().currentUser!.uid) { (favourites) in
                if favourites != nil {
                    self.artistList = favourites!.artist
                }
                
            }
        }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return artistList.count
    }
       
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       let cell = tableView.dequeueReusableCell(withIdentifier: "FavouriteCell", for: indexPath) as! FavouritesTableViewCell
       cell.configureFavouriteCell(artist: artistList[indexPath.row])
       
        return cell
    }
      
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "ArtistViewController") {
            if let viewController = segue.destination as? ArtistInformationViewController, let index =
               tableView.indexPathForSelectedRow?.row {
                viewController.cellValue = [artistList[index]]
            }
        }
    }

}
