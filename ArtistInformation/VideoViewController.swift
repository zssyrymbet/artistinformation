//
//  VideoViewController.swift
//  ArtistInformation
//
//  Created by Zarina Syrymbet on 4/29/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit
import YoutubePlayer_in_WKWebView

class VideoViewController: UIViewController {

    var videoValue: Video?
    private let manager = ArtistManager()
    @IBOutlet weak var trackImage: UIImageView!
    @IBOutlet weak var trackView: WKYTPlayerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if videoValue != nil {
            if let range = videoValue!.strMusicVid.range(of: "v=") {
                let id = videoValue!.strMusicVid[range.upperBound...]
                self.trackView.load(withVideoId: String(id))
                let url = NSURL(string: videoValue!.strMusicVid)!

                if UIApplication.shared.canOpenURL(url as URL){
                    UIApplication.shared.openURL(url as URL)
                } else{
                        let youtubeUrl = NSURL(string:"https://www.youtube.com/watch?v=\(id)")!
                    UIApplication.shared.openURL(url as URL)
                }

                if videoValue?.strTrackThumb != nil {
                    manager.loadImage(url: (videoValue?.strTrackThumb)!) { (data) in
                        self.setImage(data: data)
                    }
                }
           }
        }
    }
    
    private func setImage(data: Data) {
        trackImage.image = UIImage(data: data)
    }
  
}
