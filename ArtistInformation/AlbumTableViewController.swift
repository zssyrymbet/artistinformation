//
//  AlbumTableViewController.swift
//  ArtistInformation
//
//  Created by Zarina Syrymbet on 4/29/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit

class AlbumTableViewController: UITableViewController {

    var artistList: Artist?
    var albumList = [Album]()
    private let manager = ArtistManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if artistList != nil {
             manager.loadArtistAlbum(artistName: artistList!.strArtist, onSuccess: { (albumResponse) in
                if albumResponse.album.count != 0 {
                    self.albumList = albumResponse.album
                    self.tableView.reloadData()
                }
             }) { (error) in
                print(error)
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return albumList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AlbumCell", for: indexPath) as! AlbumTableViewCell
        cell.configureCell(album: albumList[indexPath.row])
         return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "AlbumDetailVC") {
           if let viewController = segue.destination as? AlbumDetailTableViewController, let index =
               tableView.indexPathForSelectedRow?.row {
               viewController.albumValue = albumList[index]
           }
        }
    }
}
