//
//  AlbumTableViewCell.swift
//  ArtistInformation
//
//  Created by Zarina Syrymbet on 4/29/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit

class AlbumTableViewCell: UITableViewCell {
    
    private let manager = ArtistManager()
    @IBOutlet weak var albumReleasedYear: UILabel!
    @IBOutlet weak var albumGenre: UILabel!
    @IBOutlet weak var albumName: UILabel!
    
    func configureCell(album: Album) {
        if album != nil {
            albumReleasedYear.text = album.intYearReleased
            albumGenre.text = "Genre: " + album.strGenre
            albumName.text = "Name: " + album.strAlbum
        }
    }
}
