//
//  SignInViewController.swift
//  ArtistInformation
//
//  Created by Zarina Syrymbet on 5/1/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit
import Firebase

class SignInViewController: UIViewController {

    private let manager = ArtistManager()
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func SignInButtonPressed(_ sender: Any) {
        manager.signIn(email: email.text!, password: password.text!) {
            print("SIGNIN")
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func SignUpButtonPressed(_ sender: Any) {
        func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            if(segue.identifier == "SignUpViewController") {
                
            }
        }
    }
    
}
