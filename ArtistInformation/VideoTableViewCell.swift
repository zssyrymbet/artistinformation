//
//  VideoTableViewCell.swift
//  ArtistInformation
//
//  Created by Zarina Syrymbet on 5/5/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit

class VideoTableViewCell: UITableViewCell {
    
    private let manager = ArtistManager()
    @IBOutlet weak var videoName: UILabel!
    @IBOutlet weak var videoImage: UIImageView!
    
    func configureCell(video: Video) {
        if video != nil {
            videoName.text = video.strTrack
            if video.strTrackThumb != nil {
                manager.loadImage(url: video.strTrackThumb!) { (data) in
                    self.setImage(data: data)
                }
            }
        }
    }
    
    private func setImage(data: Data) {
        videoImage.image = UIImage(data: data)
    }

}
