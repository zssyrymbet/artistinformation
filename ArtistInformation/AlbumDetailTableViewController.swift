//
//  AlbumDetailTableViewController.swift
//  ArtistInformation
//
//  Created by Zarina Syrymbet on 4/29/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit

class AlbumDetailTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var albumValue: Album?
    var trackList = [Track]()
    private let manager = ArtistManager()
    @IBOutlet weak var albumDescription: UITextView!
    @IBOutlet weak var artistName: UILabel!
    @IBOutlet weak var albumName: UILabel!
    @IBOutlet weak var albumImage: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        
        if albumValue != nil {
            manager.loadAlbumDetails(albumName: albumValue!.strAlbum, onSuccess: { (albumDetalResponse) in
                if albumDetalResponse.album.count != 0 {
                    for album in albumDetalResponse.album {
                        self.albumName.text = self.albumValue!.strAlbum
                        self.albumDescription.text = album.strDescriptionEN
                        self.artistName.text = album.strArtist
                        self.manager.loadImage(url: album.strAlbumThumb) { (data) in
                            self.setImage(data: data)
                        }
                        
                        self.manager.loadAlbumTracks(albumId: album.idAlbum, onSuccess: { (trackResponse) in
                            if trackResponse.track.count != 0 {
                                self.trackList = trackResponse.track
                                self.tableView.reloadData()
                            }
                        }) { (error) in
                            print("ERROR: ", error)
                        }
                    }
                }
            }) { (error) in
                print(error)
            }
        }
    }

    private func setImage(data: Data) {
        albumImage.image = UIImage(data: data)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return trackList.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TrackCell", for: indexPath) as! TrackTableViewCell
                cell.configureCell(track: trackList[indexPath.row])
                return cell
    }
}
