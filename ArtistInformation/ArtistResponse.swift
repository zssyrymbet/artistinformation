//
//  ArtistResponse.swift
//  ArtistInformation
//
//  Created by Zarina Syrymbet on 4/26/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit

struct Artist: Decodable {
    
    let strArtist: String
        let intBornYear: String
        let strGenre: String
        let strWebsite: String
        let strBiographyEN: String
        let strArtistThumb: String
        let strArtistLogo: String
        let idArtist: String
        
        init?(data: [String: Any]) {
            guard let strArtist = data["strArtist"] as? String, let intBornYear = data["intBornYear"] as? String, let strGenre = data["strGenre"] as? String, let strWebsite = data["strWebsite"] as? String, let strBiographyEN = data["strBiographyEN"] as? String, let strArtistThumb = data["strArtistThumb"] as? String, let idArtist = data["idArtist"] as? String, let strArtistLogo = data["strArtistLogo"] as? String else {
                    return nil
                }
            
                self.intBornYear = intBornYear
                self.strArtist = strArtist
                self.strArtistLogo = strArtistLogo
                self.strArtistThumb = strArtistThumb
                self.strBiographyEN = strBiographyEN
                self.strGenre = strGenre
                self.strWebsite = strWebsite
                self.idArtist = idArtist
        }
        
        func ArtistToDict() -> [String: Any] {
               return [
                   "intBornYear": intBornYear,
                   "strArtist": strArtist,
                   "strArtistLogo": strArtistLogo,
                   "strArtistThumb": strArtistThumb,
                   "strBiographyEN": strBiographyEN,
                   "strGenre": strGenre,
                   "strWebsite": strWebsite,
                   "idArtist":idArtist
               ]
           }
        
    }

    struct ArtistResponse: Decodable {
        let artists: [Artist]
    }

    struct Album: Decodable {
        let strAlbum: String
        let intYearReleased: String
        let strGenre: String
    }

    struct AlbumResponse: Decodable {
        let album: [Album]
    }

    struct AlbumDetails: Decodable {
        let idAlbum:  String
        let strAlbumThumb: String
        let strAlbum: String
        let strArtist: String
        let strDescriptionEN: String
    }

    struct AlbumDetailsResponse: Decodable {
        let album: [AlbumDetails]
    }

    struct Track: Decodable {
        let strTrack: String
        let intDuration: String
        let idArtist: String
    }

    struct TrackResponse: Decodable {
        let track: [Track]
    }

    struct Video: Decodable {
        let strTrack: String
        let strTrackThumb: String?
        let strMusicVid: String
    }

    struct VideoResponse: Decodable {
        let mvids: [Video]
    }




