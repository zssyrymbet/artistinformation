//
//  SignUpViewController.swift
//  ArtistInformation
//
//  Created by Zarina Syrymbet on 5/1/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController {

    private let manager = ArtistManager()
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func signUpButtonPressed(_ sender: Any) {
        print("SIGNUP")
        manager.createUser(email: email.text!, password: password.text!, username: username.text!) {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
}
