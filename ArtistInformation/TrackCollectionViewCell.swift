//
//  TrackCollectionViewCell.swift
//  ArtistInformation
//
//  Created by Zarina Syrymbet on 4/29/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit

class TrackCollectionViewCell: UITableViewCell {
    
    @IBOutlet weak var trackDuration: UILabel!
    @IBOutlet weak var trackName: UILabel!
}
