//
//  User.swift
//  ArtistInformation
//
//  Created by Zarina Syrymbet on 4/27/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit

struct User {
    var id: String
    var username: String
    var email: String
    var password: String
    
    init?(data: [String: Any]) {
        guard let id = data["id"] as? String, let username = data["username"] as? String, let email = data["email"] as? String, let password = data["password"] as? String else {
                return nil
            }
        
            self.id = id
            self.username = username
            self.email = email
            self.password = password
    }
}
