//
//  VideoTableViewController.swift
//  ArtistInformation
//
//  Created by Zarina Syrymbet on 5/5/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit

class VideoTableViewController: UITableViewController {

    var videoList = [Video]()
    private let manager = ArtistManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
            tableView.reloadData()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           return videoList.count
    }
       
       override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           let cell = tableView.dequeueReusableCell(withIdentifier: "VideoCell", for: indexPath) as! VideoTableViewCell
           cell.configureCell(video: videoList[indexPath.row])
            return cell
       }
       
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       if(segue.identifier == "VideoViewController") {
          if let viewController = segue.destination as? VideoViewController, let index =
              tableView.indexPathForSelectedRow?.row {
              viewController.videoValue = videoList[index]
          }
       }
    }
}
