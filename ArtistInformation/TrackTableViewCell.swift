//
//  TrackTableViewCell.swift
//  ArtistInformation
//
//  Created by Zarina Syrymbet on 4/30/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit

class TrackTableViewCell: UITableViewCell {

    @IBOutlet weak var trackName: UILabel!
    @IBOutlet weak var trackDuration: UILabel!
    
    func configureCell(track: Track) {
        trackName.text = track.strTrack
        let duration = track.intDuration
        let date = NSDate(timeIntervalSince1970: TimeInterval(Int(duration)! / 1000))
        let formatter = DateFormatter()
        formatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        formatter.dateFormat = "mm:ss"
        trackDuration.text = formatter.string(from: date as Date)
    }
    
}
