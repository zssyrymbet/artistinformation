//
//  ArtistInformationViewController.swift
//  ArtistInformation
//
//  Created by Zarina Syrymbet on 4/29/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit
import Firebase

class ArtistInformationViewController: UIViewController {

    var cellValue = [Artist]()
    private let manager = ArtistManager()
    @IBOutlet weak var artistBiography: UITextView!
    @IBOutlet weak var artistAge: UILabel!
    @IBOutlet weak var artistGenre: UILabel!
    @IBOutlet weak var artistImage: UIImageView!
    @IBOutlet weak var artistLogo: UIImageView!
    @IBOutlet weak var button: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if cellValue.count != 0 {
            self.title = cellValue[0].strArtist
            checkFavourites()
            artistBiography.text = cellValue[0].strBiographyEN
            let format = DateFormatter()
            format.dateFormat = "yyyy"
            let currentYear = format.string(from: Date())
            artistAge.text = "Age: " + String(Int(currentYear)! - Int(cellValue[0].intBornYear)!)
            artistGenre.text = cellValue[0].strGenre
            manager.loadImageLogo(url: cellValue[0].strArtistLogo) { (data) in
                self.setImageLogo(data: data)
            }
            manager.loadImage(url: cellValue[0].strArtistThumb) { (data) in
                self.setImage(data: data)
            }
        }
    }

    private func setImageLogo(data: Data) {
        artistLogo.image = UIImage(data: data)
    }
    
    private func setImage(data: Data) {
        artistImage.image = UIImage(data: data)
    }
    
    @IBAction func addToFavouriteButton(_ sender: Any) {
        let titleValueString = button.currentTitle!
        if titleValueString == "Remove from favourites" {
            manager.removeFromFavourites(userId: Auth.auth().currentUser!.uid, artist: self.cellValue) {
                self.button.setTitle("Add to favourites", for: .normal)
            }
        } else {
            if Auth.auth().currentUser?.uid == nil {
                self.performSegue(withIdentifier: "SignInViewController", sender: nil)
            } else {
                addToFavourites()
            }
        }
    }
               
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
            case "AlbumTableViewController":
                if let viewController = segue.destination as? AlbumTableViewController {
                    viewController.artistList = cellValue[0]
                }
            default:
                if let viewController = segue.destination as? VideoTableViewController {
                    manager.loadVideoByArtist(artistId: cellValue[0].idArtist, onSuccess: { (videoResponse) in
                        if videoResponse.mvids.count != 0 {
                            print("videoResponse", videoResponse.mvids.count)
                            viewController.videoList = videoResponse.mvids
                        }
                    }) { (error) in
                        print("ERROR: ", error)
                    }
                }
        }
    }
    
   func checkFavourites() {
       isArtistFavourite { (inFavourite) in
           if inFavourite {
               self.button.setTitle("Remove from favourites", for: .normal)
           } else {
               self.button.setTitle("Add to favourites", for: .normal)
           }
       }
   }
   
   func addToFavourites() {
       canAppenfToFavourites { (canAppend) in
           if canAppend {
               self.manager.updateFavourites(userId: Auth.auth().currentUser!.uid, artist: self.cellValue) {
                    self.button.setTitle("Remove from favourites", for: .normal)
               }
           } else {
               self.manager.saveToFavourites(userId: Auth.auth().currentUser!.uid, artist: self.cellValue) {
                    self.button.setTitle("Remove from favourites", for: .normal)
               }
           }
       }
   }
   
   func isArtistFavourite(onResult: @escaping (Bool) -> Void) {
       self.manager.getFavourites(userId: Auth.auth().currentUser!.uid) { (favourite) in
           if favourite == nil || favourite?.artist.count == 0 {
               onResult(false)
               return
           }
              
           onResult(
               favourite!.artist.contains(where: { (artist) -> Bool in
                   artist.strArtist == self.cellValue[0].strArtist }
               )
           )
       }
   }
   
   func canAppenfToFavourites(onResult: @escaping (Bool) -> Void) {
       self.manager.getFavourites(userId: Auth.auth().currentUser!.uid) { (favourite) in
           if favourite == nil {
               onResult(false)
               return
           }
                
           if favourite != nil || favourite?.artist.count ?? 0 > 0 {
               onResult(true)
               return
           }
       }
   }
}
