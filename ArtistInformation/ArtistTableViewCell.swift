//
//  ArtistTableViewCell.swift
//  ArtistInformation
//
//  Created by Zarina Syrymbet on 4/29/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit

class ArtistTableViewCell: UITableViewCell {
    
    private let manager = ArtistManager()
    @IBOutlet weak var artistLogo: UIImageView!
    @IBOutlet weak var artistName: UILabel!
    
    func configureCell(artist: Artist) {
        if artist != nil {
            artistName.text = artist.strArtist
            manager.loadImage(url: artist.strArtistThumb) { (data) in
               self.setImage(data: data)
            }
        }
    }
    
    private func setImage(data: Data) {
        artistLogo.image = UIImage(data: data)
    }
}
